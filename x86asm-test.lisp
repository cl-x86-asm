
(defpackage :cl-x86-asm-test
  (:use :cl :cl-x86-asm)
  (:nicknames :x86-asm-test))

(in-package :cl-x86-asm-test)

;; initialise our table of instructions
(make-instruction-hash-table)

;; create a code segment
(make-segment "text" :segment-type :data-segment :set-to-current t) 

;; assemble some code in it
(assemble-forms 
  '((.Entry :PUSH :EAX)
    (:SUB :EAX #XFFFEA)
    (:MOV :EAX :EBX)
    (:POP :EAX)
    (:PUSH :EAX)
    (.Exit :RET)))

;; print the assembled segment
(print-segment)

