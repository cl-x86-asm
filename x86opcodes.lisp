
(in-package :cl-x86-asm)	    

;; -- instruction encoding -----------------------------------------------------

(defun encode-32-bit-register-address (ea instruction)
  "(ecode-32-bit-register-address ea instruction) Handle the case in 
which our ea is just a register"
  (let*
      ((mod 3)
       (|r/m| (or (byte-register ea)
		  (dword-register ea)))
       (spare 
	(get-mod-rm-field :spare  
			  (zero-when-null 
			   (get instruction :|mod-rm|)))))
	 (setf (get instruction :|mod-r/m|)
	       (make-mod-rm-byte mod |r/m| spare))))

	 
(defun encode-32-bit-memory-address (ea instruction)
  "(ecode-32-bit-register-address ea instruction) Handle the case in 
which our ea is not a register but some indirect formulation of
base, index, scale, displacement etc.."
  (with-effective-address
      (size segment-reg base-reg scale index-reg 
	     displacement immediate) 
	      ea       
       (flet 
	   ((displacement-only-p ()
	      (and (not (zerop displacement)) 
		   (zerop scale) 
		   (null index-reg) 
		   (null base-reg)))
	    (base-and-displacement-only-p ()
	      (and (not (zerop displacement))
		   (not (null base-reg))
		   (null index-reg)
		   (zerop scale)))	    
	    (encode-mod ()
	      (cond
		((displacement-only-p) 0)
		((zerop displacement) 0)
		((is-byte displacement) 1)
		((is-dword displacement) 2)
		(t 
		 (error "Displacement ~X out of range" 
			displacement))))
	    (encode-rm ()
	      (cond
		((displacement-only-p) 5)
		((not (base-and-displacement-only-p)) 4)
		(t (byte-register base-reg)
		   (dword-register base-reg)))))
       (let* 
	   ((mod (encode-mod))
	    (|r/m| (encode-rm))
	    (base
	     (when (and (not (displacement-only-p))
			(not (base-and-displacement-only-p)))
	       (or (byte-register base-reg)
		   (dword-register base-reg))))
	    (index
	     (when (and (not (displacement-only-p))
			(not (base-and-displacement-only-p)))
	       (or (byte-register index-reg)
		   (dword-register index-reg))))
	    (scale 
	     (case scale
	       (0 nil)
	       (1 0)
	       (2 1)
	       (4 2)
	       (8 3))))
	 ;; body of let
	 (setf (get instruction :|mod-r/m|)
	       (make-mod-rm-byte mod |r/m| 0))
	 (setf (get instruction :sib)
	       (make-sib-byte scale index base)) 
	       instruction))))

(defun encode-16-bit-ea (instruction)
  (declare (ignore instruction))
  (error "Cannot handle 16 bit addressing yet"))

;; to do -- what about 16 bits and segment displacement and word sizes?
(defun encode-effective-address (operand instruction)
  (ccase *current-bits-size*
    ;; In 32-bit addressing mode ..
    ;; (either BITS 16 with a 67 prefix, or BITS 32 with no 67 prefix)
    ;; the general rules (again, there are exceptions) for mod and r/m are:	   
    (32 
     (cond
       ((or (byte-register operand)
	     (dword-register operand))
	(encode-32-bit-register-address operand instruction))
       ((word-register operand)
	(error "Can't handle word registers in 32 bit mode yet"))
       (t
	(progn
	  (assert (listp operand))
	  (encode-32-bit-memory-address operand instruction)))))
    ;; to be done..
    (16 (encode-16-bit-ea instruction))))

;; -- encoding method lookup table ---------------------------------------------------

(defparameter *opcode-encodings* '(:|rw/rd| :|rb| :|ow/od| :OF :/2 :|+cc|
                                  :/7 :/3 :/1 :/4 :/5 :/0 :|+r| :/6 :|ib|
                                  :|iw| :|o16| :|id| :|o32| :|/r|))

(defparameter *opcode-encoder-table* (make-hash-table))

(defun def-opcode-encoder (encoding function)
  (assert (member encoding *opcode-encodings*))
  (setf (gethash encoding *opcode-encoder-table*) function))

(defun find-operand-index (types valid-types)
  "(find-operand-types types valid-types) types is a list of
operand types in operand order, and valid-types is a list of
operand types allowed for a given encoding. Returns the index of
the operand we need to encode."
  (loop
     for operand-index = 0 then (1+ operand-index)
     for operand-type in types
     thereis (and (member operand-type valid-types) 
		  operand-index)))

;; :|+r| - one of the operands is a register and the register value
;; should be added to the appropiate opcode to produce the byte
(defun register-opcode-encoder (insn-name operands types encoding instruction)
  "(register-opcode-endoder isnn-name operands types ecoding instruction) Handles
the effect of a :|+r| encoding on an instruction."
  (declare (ignore insn-name))
  (assert (eql (first (last encoding)) :|+r|))
  (flet ((add-register-value-to-opcodes (opcodes reg-value)
	   (append
	    (subseq opcodes 0 (1- (length opcodes)))
	    (list (+ reg-value (nth (1- (length opcodes)) opcodes))))))
    (let* ((register-types '(:|reg32| :|reg16| :|reg8|))
	   (operand-index (find-operand-index types register-types))
	   (register-value (or (byte-register (nth operand-index operands))
			     (word-register (nth operand-index operands))
			     (dword-register (nth operand-index operands))))
	   (opcodes (get instruction :opcodes)))
      (when (null operand-index)
	(error  "Unable to identify operand to encode among ~A " operands))
      (setf (get instruction :opcodes)
	    (add-register-value-to-opcodes opcodes register-value)))))

;; :|+cc| condition code should be added to opcode byte - placeholder
;; that does nothing as this was expanded manually in our instruction table
(defun condition-code-opcode-encoder (insn-name operands types encoding instruction)
  (declare (ignore insn-name operands types encoding))
 instruction)

(defmacro def-slash-opcode-encoder (n &key name)
  `(defun ,name (insn-name operands types encoding instruction)
     (declare (ignore insn-name encoding))
     (let* ((ea-types 
	     '(:|mm| :|m80| :|m8| :|mem80| :|mem64| :|mem32| :|mem16| :|mem8| 
	       :|mem| :|r/m8| :|r/m16| :|r/m32| :|xmm2/m128|))
	    (operand-index (find-operand-index types ea-types))
	    (mod-rm (zero-when-null (get instruction :|mod-r/m|))))
       (when (not (numberp operand-index))
	 (error  
	  "Unable to identify operand to encode among ~A " operands))
       (encode-effective-address (nth operand-index operands) instruction)
       (setf (get instruction :|mod-r/m|) 
		  (make-mod-rm-byte 
		   (get-mod-rm-field :mod mod-rm)
		   ,n
		   (get-mod-rm-field :|r/m| mod-rm))))))

(def-slash-opcode-encoder 0 :name slash-zero-opcode-encoder)
(def-slash-opcode-encoder 1 :name slash-one-opcode-encoder)
(def-slash-opcode-encoder 2 :name slash-two-opcode-encoder)
(def-slash-opcode-encoder 3 :name slash-three-opcode-encoder)
(def-slash-opcode-encoder 4 :name slash-four-opcode-encoder)
(def-slash-opcode-encoder 5 :name slash-five-opcode-encoder)
(def-slash-opcode-encoder 6 :name slash-six-opcode-encoder)
(def-slash-opcode-encoder 7 :name slash-seven-opcode-encoder)

(defun slash-r-opcode-encoder (insn-name operands types encoding instruction)
  (declare (ignore insn-name encoding))
  (let* ((register-types
	  '(:|xmm1| :|mm1| :|reg32| :|reg16| :|reg8| :|mmxreg| :|xmm| :|mm|))
	 (ea-types 
	  '(:|mm| :|m80| :|m8| :|mem80| :|mem64| :|mem32| :|mem16| :|mem8| 
	    :|mem| :|r/m8| :|r/m16| :|r/m32| :|xmm2/m128|))
	 (register-operand-index
	   (find-operand-index types register-types))
	 (ea-operand-index 
	  (find-operand-index types ea-types))
	 (register-value 
	  (or (byte-register 
	       (nth register-operand-index operands))
	      (dword-register 
	       (nth register-operand-index operands))))
	 (mod-rm (zero-when-null (get instruction :|mod-r/m|))))
    (when (not (and (numberp register-operand-index) (numberp ea-operand-index)))
      (error  
	   "Unable to identify operand to encode among ~A " operands))
    (when (not (numberp register-value))
	(error 
	 "Unable to encode register ~A " (nth register-operand-index operands)))
    (encode-effective-address (nth ea-operand-index operands) instruction) 
    (setf (get instruction :|mod-r/m|)
	       (make-mod-rm-byte
		(get-mod-rm-field :mod mod-rm)
		register-value
		(get-mod-rm-field :|r/m| mod-rm)))))

(defun o32-opcode-encoder (insn-name operands 
			   types encoding instruction)
  (declare (ignore insn-name operands types))
  (when (member :|o32| encoding) 
     (when (= *current-bits-size* 16)
       (setf (get instruction :prefix) 
	     (list +OPERAND-SIZE-OVERRIDE-PREFIX+)))))

(defun o16-opcode-encoder (insn-name operands 
			   types encoding instruction)
  (declare (ignore insn-name operands types))
  (when (member :|o16| encoding)
    (when (= *current-bits-size* 32)
      (setf (get instruction :prefix) 
	    (list +OPERAND-SIZE-OVERRIDE-PREFIX+)))))

;; to do -- must understand symbols
(defun ib-opcode-encoder (insn-name operands 
			  types encoding instruction)
  (declare (ignore insn-name type encoding))
  (let ((operand-index (find-operand-index types '(:|imm8|))))
    (assert (not (null operand-index)))
    (setf (get instruction :immediate-data)
	       (decompose-to-n-bytes 
		(nth operand-index operands) 1))))

;; to do -- must understand symbols
(defun iw-opcode-encoder (insn-name operands 
			  types encoding instruction)
  (declare (ignore insn-name type encoding))
  (let ((operand-index 
	 (find-operand-index  types '(:|imm16|))))
    (assert (not (null operand-index)))
    (setf (get instruction :immediate-data) 
	       (decompose-to-n-bytes 
		(nth operand-index operands) 2))))

;; to do -- must understand symbols
(defun id-opcode-encoder (insn-name operands 
			  types encoding instruction)
  (declare (ignore insn-name type encoding))
  (let ((operand-index 
	 (find-operand-index types '(:|imm32|))))
    (assert (not (null operand-index)))
    (setf (get instruction :immediate-data)
	  (decompose-to-n-bytes 
	   (nth operand-index operands) 4))))

;; to do 

;; # The codes rb, rw and rd indicate that one of the operands
;; to the instruction is an immediate value, and that the difference
;; between this value and the address of the end of the instruction is
;; to be encoded as a byte, word or doubleword respectively. Where the
;; form rw/rd appears, it indicates that either rw or rd should be
;; used according to whether assembly is being performed in BITS 16 or
;; BITS 32 state respectively.

;; # The codes ow and od indicate that one of the operands to the
;; # instruction is a reference to the contents of a memory address
;; # specified as an immediate value: this encoding is used in some
;; # forms of the MOV instruction in place of the standard
;; # effective-address mechanism. The displacement is encoded as a
;; # word or doubleword. Again, ow/od denotes that ow or od should be
;; # chosen according to the BITS setting.

;; # The codes o16 and o32 indicate that the given form of the
;; # instruction should be assembled with operand size 16 or 32
;; # bits. In other words, o16 indicates a 66 prefix in BITS 32 state,
;; # but generates no code in BITS 16 state; and o32 indicates a 66
;; # prefix in BITS 16 state but generates nothing in BITS 32.

;; # The codes a16 and a32, similarly to o16 and o32, indicate the
;; # address size of the given form of the instruction. Where this
;; # does not match the BITS setting, a 67 prefix is required.

(def-opcode-encoder :|+r| #'register-opcode-encoder)
(def-opcode-encoder :|+cc| #'condition-code-opcode-encoder)
(def-opcode-encoder :/0 #'slash-zero-opcode-encoder)
(def-opcode-encoder :/1 #'slash-one-opcode-encoder)
(def-opcode-encoder :/2 #'slash-two-opcode-encoder)
(def-opcode-encoder :/3 #'slash-three-opcode-encoder)
(def-opcode-encoder :/4 #'slash-four-opcode-encoder)
(def-opcode-encoder :/5 #'slash-five-opcode-encoder)
(def-opcode-encoder :/6 #'slash-six-opcode-encoder)
(def-opcode-encoder :/7 #'slash-seven-opcode-encoder)
(def-opcode-encoder :|/r| #'slash-r-opcode-encoder)
(def-opcode-encoder :|ib| #'ib-opcode-encoder)
(def-opcode-encoder :|iw| #'iw-opcode-encoder)
(def-opcode-encoder :|id| #'id-opcode-encoder)
(def-opcode-encoder :|o32| #'o32-opcode-encoder)
(def-opcode-encoder :|o16| #'o16-opcode-encoder)
