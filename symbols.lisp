
(in-package :cl-x86-asm)

;; -- symbol table handling -----------------------------------------------------

;; symbol table maps to cl package for each segment. we maintain a list
;; of packages so that we can iterate over all symbols as needed

(defparameter *x86-symbol-packages* nil)
(defparameter *current-segment-name* nil)
(defparameter *current-segment-package* nil)

;; segments  -----------------------------------------------------


(defclass segment
    ()
  ((alignment
    :initform 2
    :initarg :alignment
    :reader alignment-of)
   (symbol-table
    :initform nil
    :accessor symbols-of))
  (:documentation "Base class for file segments"))

(defclass data-segment
    (segment)
  ((contents
    :initform (make-array '(4096)
                          :element-type '(unsigned-byte 8)
                          :fill-pointer 0)
    :accessor contents-of))
  (:documentation "An ELF segment containing actual data"))

(defclass bss-segment
    (segment)
  ((size :initform 0 :accessor bss-size-of))
  (:documentation "An ELF segment intialised at run/load time"))



(defun x86-symbol-package-name (segment-name)
  "Given a segment name, return a lisp package name to contain the symbols in the segment,"
  (concatenate 'string (string  segment-name) "-x86-symbol-package"))

;; set the current segment context
(defun set-current-segment (segment-name)
  "(set-current-segment segment-name) sets the assembler to emit code
or data into the given segement"
  (setf *current-segment-name* segment-name)
  (setf *current-segment-package*
        (find-package (x86-symbol-package-name segment-name))))

(defun fresh-package (designator)
  "Return a package if it exists, otherwise create it."
  (let ((result (find-package designator)))
    (if result
        result
        (make-package designator))))

;; create a segment, optionally making it the current segment
;; segment-type must be 'data-segment or 'bss-segment
(defun make-segment (segment-name &key set-to-current segment-type)
  "(make-segment segment-name :set-to-current t/nil segment-type) Creates
a segment for our code to be assembled into. It may be a data-segment or bss-segment
currently"
  (assert (member segment-type '(:data-segment :bss-segment)))
  (let ((segment-package (fresh-package (x86-symbol-package-name segment-name)))
        (segment-object (make-instance (find-symbol (string segment-type) :cl-x86-asm))))
    ;; add segment-package to master list
    (push segment-package *x86-symbol-packages*)
    ;; add the package to the object slot
    (setf (symbols-of segment-object) segment-package)
    ;; bind a symbol in the package to the segment object
    (setf (symbol-value (intern "segment-object" segment-package)) segment-object)
    (when set-to-current
      (set-current-segment segment-name))))

;; get the object associated with the current segment
(defun get-segment-object ()
  "(get-segment-object) Every segment has a segment object inside it's package
which we assemble actual emitted data to"
  (symbol-value  (intern "segment-object" *current-segment-package*)))

(defun get-segment-position ()
  "(get-segment-position)
returns the position the next byte will be emitted to in the current segment"
  (length (contents-of (get-segment-object))))

;; symbols

(defun make-assembler-symbol (symbol-name)
  "(make-assember-symbol symbol-name) creates a symbol with the given
name in the current segments package, and gives it a default value and type"
  (multiple-value-bind
        (package-symbol exists)
      (intern symbol-name *current-segment-package*)
    (unless exists
      (setf (symbol-value package-symbol) 0)
      (setf (get package-symbol 'reference-type) :dword))
    package-symbol))

;; add a symbol reference to a segment
(defun add-symbol-reference (symbol-name)
  "(add-symbol-reference symbol-name)
Called when the assembler finds a symbol refrence in the current
program. Adds the location of the reference to the symbol plist."
  (format *debug-io* "Referring to symbol ~A~&" symbol-name)
  (let* ((package-symbol
          (make-assembler-symbol symbol-name))
         (symbol-references
          (get package-symbol 'reference-list nil)))
    (setf (get package-symbol 'reference-list)
          (append symbol-references (get-segment-position)))))

;; set the value of a symbol
(defun add-symbol-definition (symbol-name &key (symbol-type :dword))
  "(add-symbol-reference symbol-name)
Called when the assembler finds a symbol definition in the current
segment. Sets the symbol value to the defined value and the type
to the symbol plist"
  (format *debug-io* "Defining symbol ~A~&" symbol-name)
  (let* ((package-symbol
          (make-assembler-symbol symbol-name)))
    (setf (symbol-value package-symbol) (get-segment-position))
    (setf (get package-symbol 'reference-type) symbol-type)))


(defun fixup-symbol-reference (sym-value sym-type sym-ref)
  "(fixup-symbol-reference symbol-name symbol-value symbol-type symbol-reference
Fixes up an individual reference in the segment-contents vector"
  (flet
      ;; accessors to break up type specifier (car = kind, cadr = size)
      ((reference-size (ref)
         (second ref))
       ;;    (reference-kind (ref)
       ;;      (first ref))
       )
    ;; transform symbol to sequence
    (let ((sym-value-seq
           (decompose-to-n-bytes sym-value (reference-size sym-type))))
      ;; poke sequence into contents
      (loop
         for i from 0 below (length sym-value-seq)
         do  (setf (aref (contents-of (get-segment-object))  (+ sym-ref i))
                   (elt sym-value-seq i))))))


(defun fixup-symbol-references (sym-name)
  "(fixup-symbol-references (sym-name)
Fixes up all references to a symbol in the current segment"
  (let ((sym-value (symbol-value sym-name))
        (sym-type  (get sym-name 'reference-type))
        (sym-references (get sym-name 'reference-list)))
    (mapcar
     #'(lambda (r) (fixup-symbol-reference sym-value sym-type r))
     sym-references)))


(defun fixup-segment-symbols ()
  "(fixup-segment-symbols ()
Fixup all symbols in the current segment"
  (labels ((list-segment-symbols ()
             (loop
                for sym being each present-symbol in *current-segment-package*
                collect (symbol-name sym))))
    (let ((seg-sym-names (list-segment-symbols)))
      (mapcar #'fixup-symbol-references seg-sym-names))))


;; convience function to emit stream of bytes to segment
;; (could be done with below function, but..)
(defun emit-bytes-to-segment (data)
  "(emit-bytes-to-segment data) assemble the sequence data into the current segment
as a stream of bytes"
  (map nil
       #'(lambda (x) (vector-push-extend x (contents-of (get-segment-object))))
       data))

;; emit data to our current segment
(defun emit-data-to-segment (data &key (data-size 4))
  "(emit-data-to-segment data :data-size n) Emits data to segment as a sequence
of bytes, assuming it to be of the size given, if padding or alingmnet is needed"
  (format *debug-io* "Emitting ~A to current segment~&" data)
  (labels
      ((add-to-segment-data (b)
         (let ((segment-contents
                (contents-of (get-segment-object))))
           (ctypecase b
             (integer
              (mapcar #'(lambda (x) (vector-push-extend x segment-contents))
                      (decompose-to-n-bytes b data-size)))
             (character
              (vector-push-extend (char-code b) segment-contents))
             (string
              (map nil #'add-to-segment-data b)
              (add-to-segment-data 0))))))
    ;; symbol?
    (map nil  #'add-to-segment-data data)))

(defun print-segment ()
  "(print-segment) Diagnostic function that lets us look at the contents of a segment"
  (let ((segment-object
         (get-segment-object)))
    (format t "Segment type ~A~&" (type-of segment-object))
    (ctypecase segment-object
      (data-segment
       (progn
         (format t "Segment size ~8,'0X bytes~&" (length (contents-of segment-object)))
         (loop
            for i = 0 then (1+ i)
            for bytes across (contents-of segment-object)
            do
            (if (zerop (mod i 8))
                (format t "~&~2,'0X" (aref (contents-of segment-object) i))
                (format t " ~2,'0X" (aref (contents-of segment-object) i))))))
      (bss-segment
       (format t "Bss segment containing ~8,'0X bytes~&" (bss-size-of segment-object))))))


;; all your bases are belong to us
(defun destroy-all-segments ()
  "(destroy-all-segments) Wipe everything out when we have finished"
  (loop
     for package in *x86-symbol-packages*
     do
     (delete-package package))
  (setf *current-segment-package* nil)
  (setf *current-segment-name* nil)
  (setf *x86-symbol-packages* nil))




