
(in-package :cl-x86-asm)

;; -- convienence ---------------------------------------------------------------

;;(defun dump-x86-asm-core ()
;;    (save-lisp-and-die (merge-pathnames  (make-pathname :name "sbcl.x86asm" :type ".core"))))

;; 
;; if form is nil, body is evaluated and value returned, otherwise
;; result of evaluating form is returned. Form is evaluated only once
;;
(defmacro eval-unless (form &rest body)
  (let ((form-symbol (gensym)))
    `(let ((,form-symbol ,form))
       (if ,form-symbol
	   ,form-symbol
	   ,@body))))

;; given a numeric value, potentially > 256,
;; decompose it to a list of values 0 > v > 256
(defun decompose-to-bytes (value)
  (assert (numberp value))
  (reverse
   (loop
      for x = value then (ash x -8)
      while (not (zerop x))
      collect (logand x #XFF))))

;; given a numeric value, potentially > 256,
;; decompose it to a list of values 0 > v > 256
(defun decompose-to-n-bytes (value byte-count)
  (assert (numberp value))
  (reverse
   (loop
      for x = value then (ash x -8)
      for i from 0 below byte-count
      collect (logand x #XFF))))


;; seemed like a good idea at the time..don't think I need
;; it now
(defmacro predicated-bind ((symbols-and-predicates) 
			   value-form &body form)
  "(predicated-bind ((symbol predicate ..)) value-form body)
   If the nth element of value-form satisfies the nth predicate, bind it to
   the nth symbol, else bind it to nil, then evaluate body"
  `(let
       ,(loop
           for (symbol predicate) in symbols-and-predicates
           for val in value-form
           collect 
	     (list symbol
		   (let ((val-sym (gensym)))
		     `(let ((,val-sym ,val))
			(when (funcall ,predicate ,val-sym)
			  ,val-sym)))))
     ,@form))

(defun symbol-is-in-package (sym package-name)
  "(symbol-is-in-package sym package-name) simple predicate to test what it says"
  (eq (symbol-package sym) (find-package package-name)))

(defmacro zero-when-null (sym)
  (let 
      ((sym-sym (gensym)))
    `(let
	 ((,sym-sym ,sym))
       (if (null ,sym-sym) 0 ,sym-sym))))

(defun mklist (obj)
  "Make into list if atom"
  (if (listp obj) obj (list obj)))

(defun map-and-remove-nils (fn lst)
  "Map a list by function, eliminate elements where fn returns nil"
  (let ((acc nil))
    (dolist (x lst (nreverse acc))
      (let ((val (funcall fn x)))
        (when val (push val acc))))))

(defun filter (fn lst)
  "Filter a list by function, eliminate elements where fn returns nil"
  (let ((acc nil))
    (dolist (x lst (nreverse acc))
      (when (funcall fn x)
        (push x acc)))))

(defun flatten (lis)
  "Flatten a list with sublists, and remove nils."
  (cond ((atom lis) lis)
	((listp (car lis))
	 (append (flatten (car lis)) (flatten (cdr lis))))
	(t (append (list (car lis)) (flatten (cdr lis))))))


(defun contains-keyword (keywords list)
  "Test to see if list contains one of the keywors in the list"
  (mapcar #'(lambda (k) (member k list)) keywords))
