


 ;; Lisp assembler syntax

;; (MOV :EAX #0XFF)						;; immediate
;; (MOV :EAX :EBX)							;; register
;; (MOV :EAX (:dword #X0FFF))      				;; direct
;; (MOX :EAX (:dword (+ :ES #X0FFF))) 				;; direct based
;; (MOV :EAX (:dword (+ :ES (+ :EBX (* 2 :ESI) #0XFF)))		;; indirect based scaled index offset


;; diss
;; (cond
;;   ((= mod 0)
;;    (cond
;;      ;; special cases
;;      ((= rm 5) (displacement 4))
;;      ((= rm 4) 
;;       (let ((index (decode-register (extract-index sib)))
;; 	    (dislpacement 4)))
;;       (t (displacement 0)))))
;;   ((= mod 1)
;;    (displacement 1))
;;   ((= mod 2)
;;    (displacement 4))
;;   ((not (= r/m 4)
;; 	(register (decode-register r/m))))
;;   ((= r/m 4) ;; sib is present
;;    (let
;;        ((base-reigster (decode-register (extract-base sib)))
;; 	(index-register 
;; 	  (if (not (= (extract-index sib) 4))
;; 	      (decode-register (extract-index sib))))
;; 	(scale (nth '(1 2 4 8) (extract-scale sib)))))))
 
(in-package :cl-x86-asm)	

;;; ------- opcode and address decoding related things -----------------------------

(defparameter *operand-sizes* '(:dword :word :byte))

(defparameter *opcode-encodings* '(:|rw/rd| :|rb| :|ow/od| :OF :/2 :|+cc|
                                  :/7 :/3 :/1 :/4 :/5 :/0 :|+r| :/6 :|ib|
                                  :|iw| :|o16| :|id| :|o32| :|/r|))

(defparameter *operand-types* '(:|mm/mem64| :|xmm/mem128| 
				:|xmm/mem64| :|xmm/mem32| 
				:|xmm2/mem64| :|mem80| :|mem16|
				:|fpureg| :ST0 :|memory| 
				:|imm:imm16| :|imm:imm32| 
				:|mem32| :CX :ECX 
				:|imm| :|memoffs8| 
				:|memoffs16| :|memoffs32|
				:CR0/2/3/4 :DR0/1/2/3/6/7 
				:TR3/4/5/6/7 :|xmm1/m128| 
				:|m128| :|mm2| :|mm1/m64|
				:|xmm2/m64| :|xmm1/m64| 
				:|xmm2/m32| :|xmm1/m32| 
				:|xmm2| :|xmm1/mem128|
				:|xmm2/mem128| :|xmm2/mem32| 
				:DX :|r/m64|
				:|r16/r32/m16| :|m64| :|mem64| 
				:|mmxreg|
				:|mem8| :|m8| :|mm| 
				:|xmm| :|mm2/m32| :CS
				:DS :ES :SS :FS :GS
				:|mm2/m64| :|mm1| :|mem|
				:|1| :CL :|m32| :|segreg| 
				:|m80| :|reg8|
				:|reg16| :|reg32| :|r/m8| 
				:|r/m16| :|r/m32|
				:|imm8| :AL :|imm16| :AX 
				:|imm32| :EAX
				:|xmm2/m128| :|xmm1| :|none|))

(defconstant +LOCK-PREFIX+ #XF0)
(defconstant +REPNE/REPNZ-PREFIX+ #XF2)
(defconstant +REP-PREFIX+ #XF3)
(defconstant +REPE/REPZ-PREFIX #XF3) 

(defconstant +CS-SEGMENT-OVERRIDE-PREFIX+ #X2E)
(defconstant +SS-SEGMENT-OVERRIDE-PREFIX  #X36)
(defconstant +DS-SEGMENT-OVERRIDE-PREFIX  #X3E)
(defconstant +ES-SEGMENT-OVERRIDE-PREFIX  #X26)
(defconstant +FS-SEGMENT-OVERRIDE-PREFIX  #X64)
(defconstant +GS-SEGMENT-OVERRIDE-PREFIX  #X65)

(defconstant +OPERAND-SIZE-OVERRIDE-PREFIX+ #X66)
(defconstant +ADDRESS-SIZE-OVERRIDE-PREFIX+ #X67)

(defparameter *registers-32* (list :eax :ecx :edx :ebx :esp :ebp :esi :edi))
(defparameter *registers-16* (list :ax  :cx :dx  :bx  :sp  :bi  :si  :di))
(defparameter *registers-8* (list :al :cl :dl :bl :ah :ch :dh :bh))
(defparameter *registers-mmx* (list :mm0 :mm1 :mm2 :mm3 :mm4 :mm5 :mm6 :mm7))
(defparameter *registers-xmm* (list :xmm0 :xmm1 :xmm2 :xmm3 :xmm4 :xmm5 :xmm6 :xmm7))

(defparameter *registers-segment* 
  (list :es :cs :ss :ds :fs :gs))

(defparameter *registers-fp* 
  (list :st0 :st1 :st2 :st3 :st4 :st5 :st6 :st7))

(defparameter *registers-control* 
  (list :cr0 :invalid :cr2 :cr3 :cr4))

(defparameter *registers-debug* 
  (list :dr0 :dr1 :dr2 :dr3 :invalid :invalid :dr6 :dr7))

(defparameter *registers-test* 
  (list :invalid :invalid :invalid :tr3 :tr4 :tr5 :tr6 :tr7))


;; 1 byte
(defun byte-p (x) (and (numberp x)
			  (>= x  #X-7F)
			  (< x   #X100)))

;; 2 bytes (16 bit)
(defun word-p (x) (and (numberp x)
			  (>= x #X-7FFF)
			  (< x  #X10000)))

;; 4 bytes (32 bit) == 1 float
(defun dword-p (x) (and (numberp x)
			   (>= x #X-7FFFFFF)
			   (< x  #X100000000)))

;; 8 bytes (64 bit) == 2 floats
(defun qword-p (x) (and (numberp x)
			   (>= x #X-7FFFFFFFFFFFFFFF)
			   (< x  #X10000000000000000)))

;; 16 bytes (128 bit) == 4 floats
(defun oword-p (x) (and (numberp x)
			   (>= x #X-7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
			   (< x  #X100000000000000000000000000000000)))

(defun byte-register (x) (position x *registers-8*))

(defun word-register (x) (position x *registers-16*))

(defun fpu-register (x) (position x *registers-fp*))

(defun mmx-register (x) (position x *registers-mmx*))

(defun xmm-register (x) (position x *registers-xmm*))

(defun dword-register (x) (position x *registers-32*))

(defun segment-register (x) (position x *registers-segment*))

(defun control-register (x) (position x *registers-control*))

(defun debug-register (x) (position x *registers-debug*))

(defun test-register (x) (position x *registers-test*))


(defparameter *operand-type-checkers*
  (list  
   (cons :|mm/mem64| 
	 (list (function (lambda (opcode) 
		 (eq :|mm/mem64| opcode)))))
   (cons :|xmm/mem128| 
	 (list (function (lambda (opcode) 
		 (eq :|xmm/mem128| opcode)))))
   (cons :|xmm/mem64| 
	 (list (function (lambda (opcode) 
		 (eq :|xmm/mem64| opcode)))))
   (cons :|xmm/mem32| 
	 (list (function (lambda (opcode)
		 (eq :|xmm/mem32| opcode)))))
   (cons :|xmm2/mem64| 
	 (list (function (lambda (opcode) 
		 (eq :|xmm2/mem64| opcode)))))
   (cons :|mem80| 
	 (list (function 
		(lambda (opcode) (eq :|mem80| opcode)))))
   (cons :|mem16| 
	 (list (function 
		(lambda (opcode) (eq :|mem16| opcode)))))
   (cons :|fpureg| 
	 (list (function 
		(lambda (opcode) (fpu-register opcode)))))
   (cons :ST0 
	 (list (function 
		(lambda (opcode) (eq :ST0 opcode)))))
   (cons :|memory| 
	 (list (function 
		(lambda (opcode) (eq :|memory| opcode)))))
   (cons :|imm:imm16| 
	 (list (function 
		(lambda (opcode) (eq :|imm:imm16| opcode)))))
   (cons :|imm:imm32| 
	 (list (function 
		(lambda (opcode) (eq :|imm:imm32| opcode)))))
   (cons :|mem32| 
	 (list (function 
		(lambda (opcode) (eq :|mem32| opcode)))))
   (cons :CX 
	 (list (function 
		(lambda (opcode) (eq :CX opcode)))))
   (cons :ECX 
	 (list (function 
		(lambda (opcode) (eq :ECX opcode)))))
   (cons :|imm| 
	 (list (function 
		(lambda (opcode) (numberp opcode)))))
   (cons :|memoffs8| 
	 (list (function 
		(lambda (opcode) (eq :|memoffs8| opcode)))))
   (cons :|memoffs16| 
	 (list (function 
		(lambda (opcode) (eq :|memoffs16| opcode)))))
   (cons :|memoffs32| 
	 (list (function 
		(lambda (opcode) (eq :|memoffs32| opcode)))))
   (cons :CR0/2/3/4 
	 (list (function 
		(lambda (opcode) (control-register opcode)))))
   (cons :DR0/1/2/3/6/7 
	 (list (function 
		(lambda (opcode) (debug-register opcode)))))
   (cons :TR3/4/5/6/7 
	 (list (function 
		(lambda (opcode) (test-register opcode)))))
   (cons :|xmm1/m128| 
	 (list (function 
		(lambda (opcode) (eq :|xmm1/m128| opcode)))))
   (cons :|m128| 
	 (list (function 
		(lambda (opcode) (eq :|m128| opcode)))))
   (cons :|mm2| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mm2| opcode)))))
   (cons :|mm1/m64| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mm1/m64| opcode)))))
   (cons :|xmm2/m64| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2/m64| opcode)))))
   (cons :|xmm1/m64| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm1/m64| opcode)))))
   (cons :|xmm2/m32| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2/m32| opcode)))))
   (cons :|xmm1/m32| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm1/m32| opcode)))))
   (cons :|xmm2| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2| opcode)))))
   (cons :|xmm1/mem128| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm1/mem128| opcode)))))
   (cons :|xmm2/mem128| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2/mem128| opcode)))))
   (cons :|xmm2/mem32| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2/mem32| opcode)))))
   (cons :DX 
	 (list 
	  (function 
	   (lambda (opcode) (eq :DX opcode)))))
   (cons :|r/m64| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|r/m64| opcode)))))
   (cons :|r16/r32/m16| 
	 (list 
	  (function (lambda (opcode) (eq :|r16/r32/m16| opcode)))))
   (cons :|m64| 
	 (list (function (lambda (opcode) (eq :|m64| opcode)))))
   (cons :|mem64| 
	 (list (function (lambda (opcode) (eq :|mem64| opcode)))))
   (cons :|mmxreg| 
	 (list (function (lambda (opcode) (mmx-register opcode)))))
   (cons :|mem8| 
	 (list (function (lambda (opcode) (eq :|mem8| opcode)))))
   (cons :|m8| 
	 (list (function (lambda (opcode) (eq :|m8| opcode)))))
   (cons :|mm| 
	 (list (function (lambda (opcode) (eq :|mm| opcode)))))
   (cons :|xmm| 
	 (list (function (lambda (opcode) (eq :|xmm| opcode)))))
   (cons :|mm2/m32| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mm2/m32| opcode)))))
   (cons :CS 
	 (list 
	  (function 
	   (lambda (opcode) (eq :CS opcode)))))
   (cons :DS 
	 (list 
	  (function 
	   (lambda (opcode) (eq :DS opcode)))))
   (cons :ES 
	 (list 
	  (function 
	   (lambda (opcode) (eq :ES opcode)))))
   (cons :SS 
	 (list 
	  (function 
	   (lambda (opcode) (eq :SS opcode)))))
   (cons :FS 
	 (list 
	  (function 
	   (lambda (opcode) (eq :FS opcode)))))
   (cons :GS 
	 (list 
	  (function 
	   (lambda (opcode) (eq :GS opcode)))))
   (cons :|mm2/m64| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mm2/m64| opcode)))))
   (cons :|mm1| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mm1| opcode)))))
   (cons :|mem| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|mem| opcode)))))
   (cons :|1| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|1| opcode)))))
   (cons :CL 
	 (list 
	  (function 
	   (lambda (opcode) (eq :CL opcode)))))
   (cons :|m32| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|m32| opcode)))))
   (cons :|segreg| 
	 (list 
	  (function 
	   (lambda (opcode) (segment-register opcode)))))
   (cons :|m80| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|m80| opcode)))))
   (cons :|reg8| 
	 (list 
	  (function 
	   (lambda (opcode) (byte-register opcode)))))
   (cons :|reg16| 
	 (list 
	  (function 
	   (lambda (opcode) (word-register opcode)))))
   (cons :|reg32| 
	 (list 
	  (function 
	   (lambda (opcode) (dword-register opcode)))))
   (cons :|r/m8| 
	 (list 
	  (function 
	   (lambda (opcode) 
	    (or 
	     (byte-register opcode)
	     (and (listp opcode)
		  (= (length opcode) 1)
		  (byte-register (first opcode)))
	     (and (listp opcode)
		  (eq (first opcode) :byte)))))))
   (cons :|r/m16| 
	 (list 
	  (function 
	   (lambda (opcode) 
	    (or (word-register opcode)
		(and (listp opcode)
		     (= (length opcode) 1)
		     (dword-register (first opcode)))
		(and (listp opcode)
		     (eq (first opcode) :word)))))))
   (cons :|r/m32| 
	 (list 
	  (function 
	   (lambda (opcode) 
	    (or (dword-register opcode)
		(and (listp opcode)
		     (= (length opcode) 1)
		     (dword-register (first opcode)))
		(and (listp opcode)
		     (eq (first opcode) :dword)))))))
   (cons :|imm8| 
	 (list 
	  (function 
	   (lambda (opcode) (byte-p opcode)))))
   (cons :AL 
	 (list 
	  (function 
	   (lambda (opcode) (eq :AL opcode)))))
   (cons :|imm16| 
	 (list 
	  (function 
	   (lambda (opcode) (word-p opcode)))))
   (cons :AX 
	 (list 
	  (function 
	   (lambda (opcode) (eq :AX opcode)))))
   (cons :|imm32| 
	 (list 
	  (function 
	   (lambda (opcode) (dword-p opcode)))))
   (cons :EAX 
	 (list 
	  (function (lambda (opcode) (eq :EAX opcode)))))
   (cons :|xmm2/m128| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm2/m128| opcode)))))
   (cons :|xmm1| 
	 (list 
	  (function 
	   (lambda (opcode) (eq :|xmm1| opcode)))))
   (cons :|none|
	 (list
	  (function
	   (lambda (opcode) (eq nil opcode)))))))


;; our test effective address
(defparameter *test-ea* 
  '(:dword :ES (+ :EBX (* 2 :ESI) #XFF) #XFFFF))

    		 
(defun find-size (effective-address)
  "(find-size ea) Identify the size of the thing addressed
by the ea"
  (loop 
     for element in effective-address
     thereis (and (not (listp element) )
		  (first (member element *operand-sizes*)))))



(defun find-segreg (effective-address)
  "(find-segreg ea) Given an effective address, find 
the segment register used to offset it"
  (loop
     for element in effective-address
     thereis (and (not (listp element))
		   (first 
		    (member element *registers-segment*)))))

;;
;; TO DO -- this needs to handle symbols
;;
(defun find-numeric (effective-address)
  "(find-numeric ea) Given an effective adress component
find the numeric portion of it"
  (loop
     for element in effective-address
      thereis (and
	       (numberp element) 
	       element)))

(defun find-base-list (effective-address)
  "(find-base-list  ea) Given an effective address find
the portion with the base register offset"
  (loop
     for element in effective-address
	thereis (and (listp element)
		     (eq (first element) '+)
		     element)))



(defun find-index-list (effective-address)
  "(find-index-list ea) - given an effective address,
find the list which contains the indexing portion"
  (flet ((find-index-list-aux (candidate-list)
	   (loop
	      for element in candidate-list
	      thereis (or (and (listp element) 
			       (eq (first element) '*) 
			       element)))))
    (or (find-index-list-aux effective-address)
	(find-index-list-aux 
	 (find-base-list effective-address)))))
   


(defun find-reg (base-list)
  "(find-reg base-list) - given a component of an
effective address, find the register within it"
  (loop 
     for element in base-list
     thereis (and (not (listp element))
		  (first (member element 
				 (append *registers-32* 
					 *registers-16*
					 *registers-8*))))))
		 


(defmacro with-effective-address  (symbols ea &body forms) 
  "(with-effective-address ((size-sym segment-register-sym
					     base-register-sym
					     scale-sym
					     index-register-sym
					     displacement-sym
					     immediate-sym)
					     operand &body forms)
  Pick apart an  effective address of  the  form
    (:dword :ES (+ :EBX (*  2 :ESI) #0XFF)) 
  and assign components to  the correct  symbols in the body"
  (let ((ea-sym (gensym)))
    (destructuring-bind 
	   (size-sym segreg-sym basereg-sym 
		      scale-sym indexreg-sym 
		      displacement-sym immediate-sym)
	 symbols
      `(declare (ignorable ,size-sym ,segreg-sym ,basereg-sym 
		      ,scale-sym ,indexreg-sym 
		      ,displacement-sym ,immediate-sym))
      `(let ((,ea-sym ,ea))
	 (assert (listp ,ea-sym))		 
	 (let*
	     ((,size-sym (find-size ,ea-sym))
	      (,segreg-sym (find-segreg ,ea-sym))
	      (,immediate-sym (find-numeric ,ea-sym))
	      (,basereg-sym (find-reg (find-base-list ,ea-sym)))
	      (,displacement-sym (find-numeric (find-base-list ,ea-sym)))
	      (,indexreg-sym (find-reg (find-index-list ,ea-sym)))
	      (,scale-sym (find-numeric (find-index-list ,ea-sym))))
	   ,@forms)))))
	       
		    
	
;; test case	    
;;(with-effective-address (size seg-reg base-reg scale index-reg displacement immediate) *test-ea*
;;			 (format *debug-io* "Size ~A seg-reg ~A base-reg ~A scale ~A index-reg ~A displacement ~A immediate ~A "
;;				 size seg-reg base-reg scale index-reg displacement immediate))
		
    
    
		    



	   